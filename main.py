import subprocess
from selenium import webdriver

PROFILE = r'C:\remote-profile'  # Profile path
PORT = 9222 # Remote debugging port number

# Chrome이 설치되어 있는 path
cmd = r'C:\Program Files\Google\Chrome\Application\chrome.exe'
cmd += f' --user-data-dir="{PROFILE}"'	# user-data-dir 지정
cmd += f' --remote-debugging-port={PORT}'	# remote debugging port 지정
subprocess.Popen(cmd)	# chrome 실행

chrome_options = webdriver.ChromeOptions()
chrome_options.add_experimental_option('debuggerAddress', f'127.0.0.1:{PORT}')	# 디버깅 포트로 연결
driver = webdriver.Chrome(options=chrome_options)
driver.get('https://www.naver.com')